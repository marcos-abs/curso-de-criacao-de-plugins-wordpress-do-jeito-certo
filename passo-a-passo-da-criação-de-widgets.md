# passo-a-passo da criação de widgets

## Data da ultima atualização: Wednesday, 20 October 2021 12:06:16

### 1° passo

No construtor da classe do plugin `mv-testimonials.php`, dentro do arquivo principal da função , adicione os seguinte comandos:

```php
require_once(MV_TESTIMONIALS_PATH . 'widgets/class.mv-testimonials-widget.php');
$MVTestimonialsWidget = new MV_Testimonials_Widget();

```

O resultado ficará assim, veja o destaque em vermelho, logo abaixo:

![figura1](2021-10-20-12-26-13.png)

### 2° passo

Crie o `class.mv-testimonials-widget.php` no subdiretório `widgets` dentro do diretório do plugin, como segue abaixo:

![figura2](2021-10-20-12-33-47.png)


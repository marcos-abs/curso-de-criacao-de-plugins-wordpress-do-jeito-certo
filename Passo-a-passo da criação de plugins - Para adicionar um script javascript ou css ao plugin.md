# Passo a passo para a criação de plugins - Para adicionar um script javascript ou css ao plugin

## Ultima atualização: Tuesday, 19 October 2021 14:31:28

### 1° passo

No arquivo principal (onde é criada da classe) do plugin `mv-slide.php`, adicione o gancho (hook), ao construtor da classe do plugin como mostrado abaixo:

![action](2021-10-19-14-33-45.png)

Em destaque acima a linha de comando

```php
add_action('wp_enqueue_scripts', array($this, 'register_scripts'), 999);
```

 Observação: a prioridade 999 quer dizer que ele será 999° a ser executado, para o caso de CSS, é melhor carregado por último, por que terá precedencia sobre os demais

### 2° passo

Ainda no arquivo principal do plugin `mv_slide.php`, adiciona a função abaixo:

![function](2021-10-19-14-50-21.png)

Em destaque (vermelho) o exemplo do código abaixo:

```php
    public function register_scripts() {
        wp_register_script(
            'slider-price-main-jq',
            SLIDER-PRICE_URL . 'vendor/flexslider/jquery.flexslider-min.js',
            array('jquery'),
            SLIDER-PRICE_VERSION,
            true
        );
    }
```

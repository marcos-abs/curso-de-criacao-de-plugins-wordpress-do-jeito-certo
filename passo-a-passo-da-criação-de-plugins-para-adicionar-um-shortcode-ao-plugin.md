# Passo-a-passo da criação de plugins - Para adicionar um shortcode ao plugin

## Data da ultima atualização: Tuesday, 19 October 2021 15:16:42

### 1° passo

Acrescentar no construtor da classe do plugin o código abaixo:

```php
    require_once( MV_SLIDER_PATH . 'shortcode/class.mv-slider-shortcode.php' );
            $MV_Slider_Short = new MV_Slider_shortcode();
```

### 2° passo

Acrescentar no construtor da classe do shortcode o código abaixo:

```php
add_shortcode('mv_slider', array($this, 'add_shortcode'));
```

### 3° passo

Acrescentar a função indicada no gancho (hook) do shortcode, como descrito abaixo:

```php
    public function add_shortcode($atts = array(), $content = null, $tag = '') {
        $atts = array_change_key_case( (array) $atts, CASE_LOWER); // converte os valores da variável em minúsculas (!!)
        extract(shortcode_atts(
            array(
                'id' => '',
                'orderby' => 'date'
            ),
            $atts,
            $tag
        ));
        if( !empty($id)) {
            $id = array_map('absint', explode(',', $id));
        }
        ob_start(); // sequestra toda a saída de conteudo HTML para um buffer
        require(MV_SLIDER_PATH . 'views/mv-slider_shortcode.php');
        wp_enqueue_script('mv-slider-main-jq');
        wp_enqueue_style('mv-slider-main-css');
        wp_enqueue_style('mv-slider-style-css');
        mv_slider_options();
        return ob_get_clean(); // descarrega o conteudo do buffer
    }

```

/*
 * *****
 * File: select_slider_options.sql
 * Project: Curso Criando Plugins do WordPress do Jeito Certo
 * File Created: Thursday, 14 October 2021 13:00:54
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 14 October 2021 13:01:17
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2021 - 2021 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Consulta SQL para as opções do Slider.
 * *****
 */
SELECT * FROM `wp_options` WHERE option_name LIKE "mv_slider_options";
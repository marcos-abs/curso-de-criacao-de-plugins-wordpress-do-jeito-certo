/*
 * *****
 * File: select_widget_options.sql
 * Project: Curso Criando Plugins do WordPress do Jeito Certo
 * File Created: Sunday, 24 October 2021 18:37:56
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Sunday, 24 October 2021 18:38:02
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2021 - 2021 All rights reserved, Marcant Tecnologia da Informação
 * -----
 * Description:
 * 				Consulta SQL para as opções do Widget.
 * *****
 */
SELECT * FROM `wp_options` WHERE option_name = 'widget_mv-testimonials';
# plugin-slider-flex-layout

## Tuesday, 05 October 2021 09:14:22

### Primeiro plugin

#### Compatibilidade

![compatibilidade](2021-10-05-09-15-31.png)

![estrutura de diretórios](2021-10-05-09-32-38.png)

![itens de menu](2021-10-09-12-21-36.png)

A tela acima é dos valores do argumento/atributo 'menu_position' da função 'create_post_type()'

#### Adicionando, removendo informações do banco de dados

![add_post_meta](2021-10-12-10-22-01.png)

![get_post_meta](2021-10-12-10-22-51.png)

![update_post_meta](2021-10-12-10-24-24.png)

![delete_post_meta](2021-10-12-10-25-28.png)

#### todas as operações de CRUD para os demais tipos de documento no WordPress

![todas as operações de crud](2021-10-12-10-26-44.png)

#### as operações de crud acima, na realidade, são envelopes das funções abaixo

![funções primárias de crud](2021-10-12-10-28-49.png)

#### funções para remover codigos maliciosos de origem do banco de dados

![funções de escape do php](2021-10-12-12-39-19.png)

#### função que garante uma segurança a mais para as transações entre as camadas de aplicação e usuários

![o que significa nonce](2021-10-12-14-39-52.png)

Acima, o significado da palavra "nonce"

![nonce](2021-10-12-14-35-01.png)

Acima um exemplo de geração do token de segurança, detalhes em ciano: a tag input com o tipo escondido; em vermelho: o código PHP utilizado para gerar os "nonce's"; e em verde: os ID's de identificação dos nonce's para posterior verificação

#### Filtro para mostrar as colunas dos campos texto e URL no painel de administração dos Sliders

![manage posts columns](2021-10-12-17-13-03.png)

Acima na linha 25, podemos observar o destaque em vermelho e dentro deste destaque 2 outros destaques em ciano, são as padronizações deste "filtro" o que sobra é o nome do plugin (observe bem os caracteres underline e hifens) e em verde o nome da função que será criada

#### Sintaxe das funções do filtro que adiciona os campos a visualização dos sliders

![sintaxe do array columns](2021-10-12-17-38-23.png)

Acima está a sintaxe do array columns da função chamada logo abaixo

![chamada da função](2021-10-12-17-40-56.png)

Abaixo o resultado do "filtro"

![resultado](2021-10-12-17-57-45.png)

#### Continuando com o gancho de ação para as colunas personalizadas

![sintaxe da chamada de ação](2021-10-12-17-53-00.png)

Acima está a chamada de ação que associará o gancho de ação (hook) a função que será executada

![argumentos que faltaram](2021-10-12-18-01-16.png)

Acima ficaram faltando 2 argumentos: o 1o. é a prioridade de chamada; e o 2o. é o número de argumentos que serão solicitados

#### Filtro de ordenação de campos personalizaveis

![sortable](2021-10-12-20-02-02.png)

Acima em vermelho temos o destaque do gancho, onde em verde, temos as partes que não modificam e o que sobra (dentro do destaque em vermelho) é o nome do plugin. Já em ciano, temos o nome da função que criaremos para o plugin

#### Adicionando menus ao plugin

![gancho](2021-10-13-12-39-00.png)

Acima em vermelho, o gancho de ação; e em ciano, o nome da função

#### Roles and Capabilities

![capabilities](2021-10-13-13-17-10.png)

Veja mais sobre o assunto em:

[https://wordpress.org/support/article/roles-and-capabilities/#capability-vs-role-table](WordPress Roles and Capabilities)

#### Configurações de inicialização do plugin "mv_slider"

![configurações](2021-10-13-16-12-07.png)

Acima as configurações possíveis para posicionamento no menu do WordPress.

#### Agrupamento de opções

![valores](2021-10-13-16-31-23.png)

Acima estão as configurações disponíveis para o agrupamento de opções

#### Validação de campos

![tipos possíveis](2021-10-14-19-38-40.png)

Acima em destaque estão os valores possíveis para a chave (\$key) eles assumiram os valores ID de cada elemento do array $input e os valores serão "0" ou "1" para os campos selected ou checked

#### Shortcode

![shortcode](2021-10-15-13-03-16.png)

Acima exemplo do um shortcode com seus argumentos

#### WP_Query - loop personalizado

![argumentos](2021-10-16-13-05-24.png)

Acima em vermelho, um exemplo de array utilizado para compor um loop personalizado; em verde, as especificações de tipo de postagem, de estado da publicação, identificação do registro na tabela do banco de dados, e por último, em ciano, a instância da `WP_Query()` ja com os argumentos definidos anteriormente

![loop personalizado](2021-10-16-13-23-26.png)

Acima em destaque as partes do loop personalizado completo

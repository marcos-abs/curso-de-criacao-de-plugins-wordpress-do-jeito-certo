# anotacoes-do-curso

## Wednesday, 06 October 2021 07:49:44

### como adicionar funções (plugins) no WordPress

#### sobre ganchos e filtros

![add_action and add_filter](2021-10-05-01-31-54.png)

 Exemplo acima com ganchos de ação ou add_action('hook') e com filtros ou add_filter('filter')

#### sobre filtros

![exemplo de filtro](2021-10-05-01-38-59.png)

Exemplo acima com filtro e função de filtragem

##### *** Importante! ganchos não retornam nada e filtro sem retornam algo (e isso é uma regra! não pode ser modificada, rs)

![action de exemplo para folhas de estilo](2021-10-05-01-49-32.png)

O exemplo acima é um modelo para folhas de estilo (css), como são carregadas em "cascata" utilizando a prioridade 9999 não atrapalha o carregamento da página

![Exemplo de filtro](2021-10-05-01-58-53.png)

O exemplo acima é de uma função de filtro que contem uma função que substitui duas tags `<br/>` por um paragrafo em branco `<p></p>`

![gancho de ação](2021-10-05-02-02-44.png)

No exemplo acima, a função `do_action()` cria um gancho de ação e pode ser em qualquer lugar do código, porém existe uma lógica para o lugar dele.

![filtros](2021-10-05-02-15-59.png)

No exemplo acima, a função `apply_filters()` define o filtro e a `add_filter()` adiciona o filtro para sua execução

![retorno constante](2021-10-05-23-49-34.png)

Na tela acima, está o resultado da linha de comando aqui abaixo:

`define('MV_SLIDER_PATH', plugin_dir_path(__FILE__));`

Veja o exemplo abaixo no arquivo `mv-slider.php`:

![exemplo plugin_dir_path](2021-10-05-23-55-07.png)

Acima em destaque de vermelho, a função que define a constante, e em ciano a classe do plugin

![register_hook](2021-10-06-07-46-28.png)

Na figura acima, `register_activation_hook()` habilita recursos na sua ativação, `register_deactivation_hook()` desabilitar recursos na sua desativação e `register_uninstall_hook()` remove recursos na sua desinstalação

#### Telas sem comentários ainda

##### 4. Projeto #2 - MV Testimonials

###### 7. API Widget - Criando o formulário do widget

![4m37s](2021-10-23-13-37-29.png)

![6m26s](2021-10-23-13-45-42.png)

##### 10. API Widget - Mostrando o widget no frontend (parte 2)

![7m12s](2021-10-24-19-20-26.png)

##### 15. Criando templates especializados para o plugin (parte 4)

![5m13s](2021-10-25-23-21-30.png)

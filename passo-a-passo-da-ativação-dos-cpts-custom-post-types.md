# Passo-a-passo da ativação dos CPTs - Custom Post Types

## Data da ultima atualização: Tuesday, 19 October 2021 23:49:56

### 1° passo

Criar um arquivo no subdiretório do plugin `post-types` como no exemplo: `class.mv-testimonials-cpt.php`

### 2° passo

Abra o novo arquivo `class.mv-testimonials-cpt.php` e adicione a classe conforme o exemplo abaixo:

```php
if(!class_exists('MV_Testimonials_Post_Type')) {
    class MV_Testimonials_Post_Type {
        public function __construct() {

        }
    }
}
```

### 3° passo

Adicione ao construtor da classe do plugin `MV_Testimonials` o arquivo contendo a classe do CPT (Custom Post Type), conform exemplo abaixo:

```php
require_once(MV_TESTIMONIALS_PATH . 'post-types/class.mv-testimonials-cpt.php');
$MVTestimonialsPostType = new MV_Testimonials_Post_Type();
```

### 4° passo

Retornando ao arquivo da class do CPT, adicione o gancho (hook) de ação para registrar o novo CPT, conforme exemplo abaixo:

![figura1](2021-10-20-00-08-46.png)

Em destaque (vermelho) o comando para adicionar o recurso (veja abaixo), e em ciano, o nome da função que criará o CPT, veja abaixo:

```php
add_action('init', array($this, 'create_post_type'));
```

### 5° passo

Dentro da função `create_post_type()`, registre o CPT (Custom Post Type), como segue o código abaixo:

```php
register_post_type(
    'mv-testimonials',
    array(
        'label' => esc_html__('Testimonial', 'mv-testimonials'),
        'description'   => esc_html__('Testimonials', 'mv-testimonials'),
        'labels' => array(
            'name'  => esc_html__('Testimonials', 'mv-testimonials'),
            'singular_name' => esc_html__('Testimonial', 'mv-testimonials')
        ),
        'public'    => true,
        'supports'  => array('title', 'editor', 'thumbnail'),
        'hierarchical'  => false,
        'show_ui'   => true,
        'show_in_menu'  => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export'    => true,
        'has_archive'   => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'show_in_rest'  => true,
        'menu_icon' => 'dashicons-testimonial',
    )
);
```

### 6° passo

Criar as funções de ativação e desativação

```php
public static function activate(){
    update_option('rewrite_rules', '' );
}

public static function deactivate(){
    flush_rewrite_rules();
    unregister_post_type( 'mv-testimonials' );
}
```
